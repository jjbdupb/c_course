#include <stdio.h>

int isPrime(int);


int main()
{
	for (int i = 0; i < 100; ++i)
	{
		if(isPrime(i)){
			printf("%d\n", i);
		}	
	}
	return 0;
}


int isPrime(int x){

	if(x == 0 || x == 1){
		return 0;
	}
	if(x == 2){
		return 1;
	}

	for (int i = 2; i <x ; ++i)
	{
		if(x%i == 0){
			return 0;
		}
	}

	return 1;
}