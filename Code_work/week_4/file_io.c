#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

int main()
{
	FILE * fp ;
	char ch ;
	if ((fp = fopen("myFile.txt" , "r")) == NULL)
	{
		printf ("Unable to open file for read access .\n");
		fprintf (stderr, "error %d: %s\n", errno, strerror(errno));
		exit(1);
	}
	while (!feof(fp))
	{
		ch = fgetc(fp);
		printf("%3 d: %c \n", ch, ch);
	}
	fclose(fp);
}
